import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { fetchData, postData } from "./todoApi";

const initialState = {
  data: [],
  loading: false,
  error: null,
};

// fetch todo list
export const getTodo = createAsyncThunk("todo/getData", async () => {
  const res = await fetchData();
  return res;
});

// add todo list
export const addTodo = createAsyncThunk("todo/addData", async (payload) => {
  const res = await postData(payload);
  return res;
});

export const todoSlice = createSlice({
  name: "todo",
  initialState,
  extraReducers: (builder) => {
    builder
      // fetch todo
      .addCase(getTodo.pending, (state) => {
        state.error = null;
        state.loading = true;
        state.data = [];
      })
      .addCase(getTodo.fulfilled, (state, action) => {
        state.data = action.payload;
        state.loading = false;
      })
      .addCase(getTodo.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
        state.data = [];
      })
      //add todo
      .addCase(addTodo.pending, (state) => {
        state.error = null;
        state.loading = true;
      })
      .addCase(addTodo.fulfilled, (state, action) => {
        state.data.unshift(action.payload);
        state.loading = false;
      })
      .addCase(addTodo.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      });
  },
});

export default todoSlice.reducer;
