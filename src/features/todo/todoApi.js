export const fetchData = async () => {
  try {
    const res = await fetch("https://jsonplaceholder.typicode.com/todos");
    return res?.json();
  } catch (error) {
    throw error;
  }
};

export const postData = async (payload) => {
  try {
    const res = await fetch("https://jsonplaceholder.typicode.com/todos", {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
    return res?.json();
  } catch (error) {
    throw error;
  }
};
