export const fetchData = async () => {
  try {
    const res = await fetch("https://jsonplaceholder.typicode.com/todos");
    return res?.json();
  } catch (error) {
    throw error;
  }
};
