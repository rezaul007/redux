import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { fetchData } from "./fetchDataApi";

const initialState = {
  data: [],
  loading: false,
  error: null,
};

export const getData = createAsyncThunk("fetchData/getData", async () => {
  const res = await fetchData();
  return res;
});

export const fetchDataSlice = createSlice({
  name: "fetchData",
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getData.pending, (state) => {
        state.error = null;
        state.loading = true;
        state.data = [];
      })
      .addCase(getData.fulfilled, (state, action) => {
        state.data = action.payload;
        state.loading = false;
      })
      .addCase(getData.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      });
  },
});

export default fetchDataSlice.reducer;
