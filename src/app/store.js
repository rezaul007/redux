import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import fetchDataReducer from "../features/fetchData/fetchDataSlice";
import todoReducer from "../features/todo/todoSlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    fetchData: fetchDataReducer,
    todo: todoReducer,
  },
});
