import React, { useState } from "react";

export const Form = ({ submitHandler, todoTitle = "", todoStatus = false }) => {
  const [title, setTitle] = useState(todoTitle);
  const [status, setStatus] = useState(todoStatus);

  return (
    <form
      onSubmit={(e) => submitHandler(e, { title, status })}
      className="needs-validation"
      noValidate
    >
      <div className="mb-3">
        <div className="mb-3">
          <label htmlFor="title" className="form-label">
            Title
          </label>
          <input
            required
            type="text"
            className={`form-control ${
              title !== "" ? "is-valid" : "is-invalid"
            }`}
            id="title"
            placeholder="Todo title"
            name="title"
            autoComplete="off"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <div className="invalid-feedback">Required</div>
        </div>
      </div>
      <div className="mb-3">
        <label className="form-label">Status</label>
        <div className="input-group">
          <div className="form-check me-3 ">
            <input
              className="form-check-input"
              type="radio"
              name="completed"
              id="completed1"
              value={true}
              onChange={(e) => {
                setStatus(true);
              }}
              checked={status === true}
            />
            <label className="form-check-label" htmlFor="completed1">
              Completed
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="completed"
              id="completed2"
              value={false}
              onChange={(e) => {
                setStatus(false);
              }}
              checked={status === false}
            />
            <label className="form-check-label" htmlFor="completed2">
              Not Completed
            </label>
          </div>
        </div>
      </div>

      <button type="submit" className="btn btn-primary btn-sm ">
        Submit
      </button>
    </form>
  );
};
