import React from "react";

const Table = ({ data = [] }) => {
  return (
    <table className="table table-bordered">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Title</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        {data?.map((item, index) => (
          <tr key={index}>
            <th scope="row">{index + 1}</th>
            <td>{item?.title}</td>
            <td>{item?.status}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
