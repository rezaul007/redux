import React, { useEffect } from "react";
import { Form } from "./component/Form";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Table from "./component/Table";
import { useDispatch, useSelector } from "react-redux";
import { addTodo, getTodo } from "./features/todo/todoSlice";

function App() {
  const dispatch = useDispatch();
  const { data, loading } = useSelector((store) => store.todo);

  useEffect(() => {
    dispatch(getTodo());
  }, [dispatch]);

  const handleSubmit = (event, values) => {
    event.preventDefault();
    dispatch(addTodo(values));
  };
  return (
    <div className="App container">
      <div className="row mt-4">
        <div className="col-sm-8 col-lg-6  offset-lg-3 offset-sm-2">
          <div className="card">
            <div className="card-body">
              <Form submitHandler={handleSubmit} />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-sm-8 col-lg-6  offset-lg-3 offset-sm-2">
          {loading ? (
            <div className="d-flex justify-content-center mt-3">
              <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
          ) : (
            <Table data={data} />
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
